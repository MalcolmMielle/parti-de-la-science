---
layout: default
---

<!-- <div class="home">

  <h1 class="page-heading">Posts</h1>

  <ul class="post-list">
    {% for post in site.posts %}
      <li>
        <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }}</span>

        <h2>
          <a class="post-link" href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a>
        </h2>
      </li>
    {% endfor %}
  </ul>

  <p class="rss-subscribe">subscribe <a href="{{ "/feed.xml" | prepend: site.baseurl }}">via RSS</a></p>

</div> -->

# Party line: le parti des bienpensants.

Une description de mon parti politique parce que si les autres font n'importe quoi je vois pas pourquoi je ferais pas n'importe quoi non plus.

# Ce que nous sommes

Un parti qui se focalise sur la science pour la socitété, un parti contre le rechauffement climatique, et un parti pour une meilleur éducation et un système plus juste.

# Ce que nous ne sommes pas

Un parti qui prétend fédérer toutes les francaise et francais.
Nous sommes un partie sans tolérance pour l'ignorance assumées.
Nous n'avons aucun soucis avec les erreurs, la volonté d'apprendre, mais nous n'allons pas chercher a convaincre les gens convaincu d'idiotie (je vous regarde les anti-vaccins).

# Mesures phares

1. *Le vote des députés necessitera un minimum de connaissance*. Après tout, on demande ca pour n'importe quel autre travail à l'embauche donc c'est pas fou! Si un sujet de loi touche a un sujet scientifique (par exemple, rechauffement climatique ou épidémiologie) les députés de l'assemblé nationale doivent soit:
    * Justifier de connaissance récente par rapport au sujet (diplome ou pratique scientifique professionelle dans les 3 dernières années )
    * Passer un examen sur le sujet justifiant de connaissance basique. Ce test sera créé par un comité scientifique d'experts sur le sujet et le resultat sera valide pour une durée d'un an.

2. *On arrete d'ecouter les boomer sur le rechauffement climatique s'ils disent que ca n'existe pas*.
Le rechauffement climatique c'est pas des conneries alors pourquoi on "débat" encore la dessus?

3. *On financera les transports en commun avec des taxes, on interdira la voiture en ville, et on rendra les entreprises qui vendent des produits plastique responsable de leur recyclage* avec des enorme amendes s'il n'est pas recyclé (ouais on le sait que la majorité des plastiques sont pas recyclables).

4. *On calculera notre production de CO2 en prenant en compte les cout de production de nos produits a l'etranger.*
On arrete la triche avec des "la France polue pas"... vu que d'autre pays poluent pour nous.

5. *Les praticiens de pseudo-science du type osteopathie, naturopathie, et homoepathie ne seront plus couvert par la sécu s'ils ont un problème* à moins d'avoir fait des études reconnues par l'ordre des médecins.
La société c'est aussi des responsabilités: quand tu rend le travail de l'hopital plus dur, le minimum c'est que tu débourse de ta poche quand tu as un problème.

6. *On fait repasser le BAC scientifique à toute la France.*
Ca n'aura aucune conséquence mais peut être que ca calmera les gens qui pensent qu'avec 5min de recherche sur facebook ils en savent autant qu'un scientifique ayant dédié sa vie à la question.
Et on saura enfin si "de mon temps c'etait plus dur" ou si c'est juste l'ego qui parle (narrateur: c'etait l'ego).
